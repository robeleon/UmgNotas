/*
 * This source file was generated by FireStorm/DAO.
 * 
 * If you purchase a full license for FireStorm/DAO you can customize this header file.
 * 
 * For more information please visit http://www.codefutures.com/products/firestorm
 */

package umgnotas.eis.dao;

import java.sql.Connection;
import java.util.Date;
import umgnotas.eis.dto.*;
import umgnotas.eis.exceptions.*;

public interface TbEstudianteDao
{
	/** 
	 * Inserts a new row in the tb_estudiante table.
	 */
	public TbEstudiantePk insert(TbEstudiante dto) throws TbEstudianteDaoException;

	/** 
	 * Updates a single row in the tb_estudiante table.
	 */
	public void update(TbEstudiantePk pk, TbEstudiante dto) throws TbEstudianteDaoException;

	/** 
	 * Deletes a single row in the tb_estudiante table.
	 */
	public void delete(TbEstudiantePk pk) throws TbEstudianteDaoException;

	/** 
	 * Returns the rows from the tb_estudiante table that matches the specified primary-key value.
	 */
	public TbEstudiante findByPrimaryKey(TbEstudiantePk pk) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_carne = :estCarne'.
	 */
	public TbEstudiante findByPrimaryKey(String estCarne) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria ''.
	 */
	public TbEstudiante[] findAll() throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_usu_ingreso = :estUsuIngreso'.
	 */
	public TbEstudiante[] findByTbUsuario(String estUsuIngreso) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_sed_codigo = :estSedCodigo'.
	 */
	public TbEstudiante[] findByTbSede(int estSedCodigo) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_carne = :estCarne'.
	 */
	public TbEstudiante[] findWhereEstCarneEquals(String estCarne) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_primer_nombre = :estPrimerNombre'.
	 */
	public TbEstudiante[] findWhereEstPrimerNombreEquals(String estPrimerNombre) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_segundo_nombre = :estSegundoNombre'.
	 */
	public TbEstudiante[] findWhereEstSegundoNombreEquals(String estSegundoNombre) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_primer_apellido = :estPrimerApellido'.
	 */
	public TbEstudiante[] findWhereEstPrimerApellidoEquals(String estPrimerApellido) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_segundo_apellido = :estSegundoApellido'.
	 */
	public TbEstudiante[] findWhereEstSegundoApellidoEquals(String estSegundoApellido) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_fecha_nacimiento = :estFechaNacimiento'.
	 */
	public TbEstudiante[] findWhereEstFechaNacimientoEquals(String estFechaNacimiento) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_sexo = :estSexo'.
	 */
	public TbEstudiante[] findWhereEstSexoEquals(String estSexo) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_estado = :estEstado'.
	 */
	public TbEstudiante[] findWhereEstEstadoEquals(String estEstado) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_fecha_ingreso = :estFechaIngreso'.
	 */
	public TbEstudiante[] findWhereEstFechaIngresoEquals(String estFechaIngreso) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_usu_ingreso = :estUsuIngreso'.
	 */
	public TbEstudiante[] findWhereEstUsuIngresoEquals(String estUsuIngreso) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the criteria 'est_sed_codigo = :estSedCodigo'.
	 */
	public TbEstudiante[] findWhereEstSedCodigoEquals(int estSedCodigo) throws TbEstudianteDaoException;

	/** 
	 * Sets the value of maxRows
	 */
	public void setMaxRows(int maxRows);

	/** 
	 * Gets the value of maxRows
	 */
	public int getMaxRows();

	/** 
	 * Returns all rows from the tb_estudiante table that match the specified arbitrary SQL statement
	 */
	public TbEstudiante[] findByDynamicSelect(String sql, Object[] sqlParams) throws TbEstudianteDaoException;

	/** 
	 * Returns all rows from the tb_estudiante table that match the specified arbitrary SQL statement
	 */
	public TbEstudiante[] findByDynamicWhere(String sql, Object[] sqlParams) throws TbEstudianteDaoException;

	public Connection getUserConn();

	public void setUserConn(Connection userConn);

}
