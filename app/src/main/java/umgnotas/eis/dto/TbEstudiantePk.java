/*
 * This source file was generated by FireStorm/DAO.
 * 
 * If you purchase a full license for FireStorm/DAO you can customize this header file.
 * 
 * For more information please visit http://www.codefutures.com/products/firestorm
 */

package umgnotas.eis.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
 * This class represents the primary key of the tb_estudiante table.
 */
public class TbEstudiantePk implements Serializable
{
	protected String estCarne;

	/** 
	 * Sets the value of estCarne
	 */
	public void setEstCarne(String estCarne)
	{
		this.estCarne = estCarne;
	}

	/** 
	 * Gets the value of estCarne
	 */
	public String getEstCarne()
	{
		return estCarne;
	}

	/**
	 * Method 'TbEstudiantePk'
	 * 
	 */
	public TbEstudiantePk()
	{
	}

	/**
	 * Method 'TbEstudiantePk'
	 * 
	 * @param estCarne
	 */
	public TbEstudiantePk(final String estCarne)
	{
		this.estCarne = estCarne;
	}

	/**
	 * Method 'equals'
	 * 
	 * @param _other
	 * @return boolean
	 */
	public boolean equals(Object _other)
	{
		if (_other == null) {
			return false;
		}
		
		if (_other == this) {
			return true;
		}
		
		if (!(_other instanceof TbEstudiantePk)) {
			return false;
		}
		
		final TbEstudiantePk _cast = (TbEstudiantePk) _other;
		if (estCarne == null ? _cast.estCarne != estCarne : !estCarne.equals( _cast.estCarne )) {
			return false;
		}
		
		return true;
	}

	/**
	 * Method 'hashCode'
	 * 
	 * @return int
	 */
	public int hashCode()
	{
		int _hashCode = 0;
		if (estCarne != null) {
			_hashCode = 29 * _hashCode + estCarne.hashCode();
		}
		
		return _hashCode;
	}

	/**
	 * Method 'toString'
	 * 
	 * @return String
	 */
	public String toString()
	{
		StringBuffer ret = new StringBuffer();
		ret.append( "umgnotas.eis.dto.TbEstudiantePk: " );
		ret.append( "estCarne=" + estCarne );
		return ret.toString();
	}

}
