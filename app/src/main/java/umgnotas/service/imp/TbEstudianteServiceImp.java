package umgnotas.service.imp;

import umgnotas.eis.dao.TbEstudianteDao;
import umgnotas.service.TbEstudianteService;

/**
 * Created by marvinmanuelmenchumenchu on 28/07/17.
 */

public class TbEstudianteServiceImp implements TbEstudianteService {

    private static TbEstudianteService tbEstudianteServiceInstance;
    TbEstudianteDao tbEstudianteDao;


    private TbEstudianteServiceImp() {

    }

    public static TbEstudianteService getInstance() {
        if (tbEstudianteServiceInstance == null) {
            tbEstudianteServiceInstance = new TbEstudianteServiceImp();
        }
        return tbEstudianteServiceInstance;
    }
}
