package umgnotas.service.imp;

import umgnotas.eis.dao.TbCursoDao;
import umgnotas.service.TbCarreraService;
import umgnotas.service.TbCursoService;

/**
 * Created by marvinmanuelmenchumenchu on 28/07/17.
 */

public class TbCursoServiceImp implements TbCursoService {

    private static TbCursoService tbCursoServiceInstance;
    TbCursoDao tbUsuarioDao;


    private TbCursoServiceImp() {

    }

    public static TbCursoService getInstance() {
        if (tbCursoServiceInstance == null) {
            tbCursoServiceInstance = new TbCursoServiceImp();
        }
        return tbCursoServiceInstance;
    }
}
