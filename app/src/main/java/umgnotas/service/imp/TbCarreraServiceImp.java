package umgnotas.service.imp;

import umgnotas.eis.dao.TbCarreraDao;
import umgnotas.service.TbCarreraService;

/**
 * Created by marvinmanuelmenchumenchu on 28/07/17.
 */

public class TbCarreraServiceImp implements TbCarreraService {

    private static TbCarreraService tbCarreraServiceInstance;
    TbCarreraDao tbCarreraDao;


    private TbCarreraServiceImp() {

    }

    public static TbCarreraService getInstance() {
        if (tbCarreraServiceInstance == null) {
            tbCarreraServiceInstance = new TbCarreraServiceImp();
        }
        return tbCarreraServiceInstance;
    }
}
