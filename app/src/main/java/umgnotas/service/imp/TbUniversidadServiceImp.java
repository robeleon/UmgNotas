package umgnotas.service.imp;

import umgnotas.eis.dao.TbUniversidadDao;
import umgnotas.eis.dto.TbUniversidad;
import umgnotas.eis.exceptions.TbUniversidadDaoException;
import umgnotas.eis.factory.TbUniversidadDaoFactory;
import umgnotas.eis.jdbc.ResourceManager;
import umgnotas.service.TbUniversidadService;
import umgnotas.service.exceptions.BusinessException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by marvinmanuelmenchumenchu on 25/07/17.
 */

public class TbUniversidadServiceImp implements TbUniversidadService {

    private static TbUniversidadService TbUniversidadServiceInstance;
    TbUniversidadDao tbUniversidadDao;


    private TbUniversidadServiceImp() {

    }

    public static TbUniversidadService getInstance() {
        if (TbUniversidadServiceInstance == null) {
            TbUniversidadServiceInstance = new TbUniversidadServiceImp();
        }
        return TbUniversidadServiceInstance;
    }


    @Override
    public boolean guardarUniversidad(TbUniversidad tbUniversidad) {
        Connection conn = null;
        try {
            this.tbUniversidadDao = TbUniversidadDaoFactory.create();
            conn = ResourceManager.getConnection();
            conn.setAutoCommit(false);
            this.tbUniversidadDao.setUserConn(conn);
            if (tbUniversidad.getUniCodigo() == null) {
                tbUniversidad.setUniCodigo(1);
                this.tbUniversidadDao.insert(tbUniversidad);
            } else {
                this.tbUniversidadDao.update(tbUniversidad.createPk(), tbUniversidad);
            }
            conn.commit();
            return true;
        } catch (TbUniversidadDaoException ex) {
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                throw new BusinessException("No se pudo reestablecer el estado de la Base de Datos", ex1);
            }
            throw new BusinessException("No se puedo agregar universidad: " + tbUniversidad + " a la BD", ex);
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                throw new BusinessException("No se pudo reestablecer el estado de la Base de Datos", ex1);
            }
            throw new BusinessException("Existe un problema con la Base de Datos", ex);
        } finally {
            ResourceManager.close(conn);
        }
    }

    @Override
    public boolean verificarUniversidad() {

        try {
            this.tbUniversidadDao = TbUniversidadDaoFactory.create();
            final String SQL_WHERE = " UNI_CODIGO = ? ";
            Object[] sqlParams = {1};
            TbUniversidad[] universidad = this.tbUniversidadDao.findByDynamicWhere(SQL_WHERE, sqlParams);
            if (universidad.length > 0) {
                return true;
            }
        } catch (TbUniversidadDaoException ex) {
            throw new BusinessException("Existe un problema al obtener la universidad en la BD", ex);
        }
        return false;
    }
}
