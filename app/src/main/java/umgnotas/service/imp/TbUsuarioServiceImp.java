package umgnotas.service.imp;

import java.sql.Connection;
import java.sql.SQLException;

import umgnotas.eis.dao.TbUsuarioDao;
import umgnotas.eis.dto.TbUsuario;
import umgnotas.eis.exceptions.TbUsuarioDaoException;
import umgnotas.eis.factory.TbUsuarioDaoFactory;
import umgnotas.eis.jdbc.ResourceManager;
import umgnotas.service.TbUsuarioService;
import umgnotas.service.exceptions.BusinessException;

/**
 * Created by marvinmanuelmenchumenchu on 26/07/17.
 */

public class TbUsuarioServiceImp implements TbUsuarioService {

    private static TbUsuarioService tbUsuarioServiceInstance;
    TbUsuarioDao tbUsuarioDao;


    private TbUsuarioServiceImp() {

    }

    public static TbUsuarioService getInstance() {
        if (tbUsuarioServiceInstance == null) {
            tbUsuarioServiceInstance = new TbUsuarioServiceImp();
        }
        return tbUsuarioServiceInstance;
    }


    @Override
    public boolean guardarUsuario(TbUsuario tbUsuario) {
        Connection conn = null;
        try {
            this.tbUsuarioDao = TbUsuarioDaoFactory.create();
            conn = ResourceManager.getConnection();
            conn.setAutoCommit(false);
            this.tbUsuarioDao.setUserConn(conn);
                this.tbUsuarioDao.insert(tbUsuario);
            conn.commit();
            return true;
        } catch (TbUsuarioDaoException ex) {
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                throw new BusinessException("No se pudo reestablecer el estado de la Base de Datos", ex1);
            }
            throw new BusinessException("No se puedo agregar usuario: " + tbUsuario + " a la BD", ex);
        } catch (SQLException ex) {
            try {
                conn.rollback();
            } catch (SQLException ex1) {
                throw new BusinessException("No se pudo reestablecer el estado de la Base de Datos", ex1);
            }
            throw new BusinessException("Existe un problema con la Base de Datos", ex);
        } finally {
            ResourceManager.close(conn);
        }
    }

    @Override
    public boolean existeUsuario() {
        try {
            this.tbUsuarioDao = TbUsuarioDaoFactory.create();
            final String SQL_WHERE = " USU_CODIGO = ? ";
            Object[] sqlParams = {"admin"};
            TbUsuario[] sede = this.tbUsuarioDao.findByDynamicWhere(SQL_WHERE, sqlParams);
            if (sede.length > 0) {
                return true;
            }
        } catch (TbUsuarioDaoException ex) {
            throw new BusinessException("Existe un problema al obtener la sede en la BD", ex);
        }
        return false;
    }

    @Override
    public boolean validarUsuario(TbUsuario tbUsuario) {
        try {
            this.tbUsuarioDao = TbUsuarioDaoFactory.create();
            final String SQL_WHERE = " USU_CODIGO = ? AND USU_PASSWORD = ?";
            Object[] sqlParams = {tbUsuario.getUsuCodigo(), tbUsuario.getUsuPassword()};
            TbUsuario[] usuarioExiste = this.tbUsuarioDao.findByDynamicWhere(SQL_WHERE, sqlParams);
            if (usuarioExiste.length > 0) {
                return true;
            }
        } catch (TbUsuarioDaoException ex) {
            throw new BusinessException("Existe un problema al obtener usuario en la BD", ex);
        }
        return false;
    }
}
