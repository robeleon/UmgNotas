package umgnotas.service.imp;

import umgnotas.eis.dao.TbPagosDao;
import umgnotas.service.TbPagosService;

/**
 * Created by marvinmanuelmenchumenchu on 28/07/17.
 */

public class TbPagosServiceImp implements TbPagosService {

    private static TbPagosService tbPagosServiceInstance;
    TbPagosDao tbUsuarioDao;


    private TbPagosServiceImp() {

    }

    public static TbPagosService getInstance() {
        if (tbPagosServiceInstance == null) {
            tbPagosServiceInstance = new TbPagosServiceImp();
        }
        return tbPagosServiceInstance;
    }
}
