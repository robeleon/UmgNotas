package umgnotas.service;

import umgnotas.eis.dto.TbSede;

/**
 * Created by marvinmanuelmenchumenchu on 25/07/17.
 */

public interface TbSedeService {

    public boolean guardarSede(TbSede tbSede);

    public boolean verificarSede();

}
