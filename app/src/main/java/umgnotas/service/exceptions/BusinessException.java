package umgnotas.service.exceptions;

/**
 * Created by marvinmanuelmenchumenchu on 25/07/17.
 */

public class BusinessException extends RuntimeException {
    public BusinessException (String mensaje, Exception e){
        super(mensaje, e);
    }
}
