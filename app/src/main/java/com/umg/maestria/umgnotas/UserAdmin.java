package com.umg.maestria.umgnotas;

import android.content.Intent;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import umgnotas.eis.dto.TbUsuario;
import umgnotas.service.TbUsuarioService;
import umgnotas.service.imp.TbUsuarioServiceImp;

public class UserAdmin extends AppCompatActivity {

    private EditText pass;
    private EditText passC;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_admin);

        pass = (EditText) findViewById(R.id.pass1);
        passC = (EditText) findViewById(R.id.pass2);
    }

    public void guardarUsuarioAdmin(View view){

        String password = pass.getText().toString();
        String confirmacion = passC.getText().toString();

        if (password.equals(confirmacion)){
            TbUsuario tbUsuario = new TbUsuario();
            tbUsuario.setUsuCodigo("admin");
            tbUsuario.setUsuNombre("Administrador");
            tbUsuario.setUsuApellido("");

            tbUsuario.setUsuPassword(MD5(password));
            tbUsuario.setUsuRol("A");
            tbUsuario.setUsuSedCodigo(1);

            //Llamo los servicios de Usuario
            TbUsuarioService tbUsuarioService = TbUsuarioServiceImp.getInstance();
            boolean guardado = tbUsuarioService.guardarUsuario(tbUsuario);
            if(guardado){
                finish();
                Intent e = new Intent(this, Escritorio.class);
                e.putExtra("usuario", "admin");
                startActivity(e);
                Toast notification = Toast.makeText(this, "Usuario Administrador creada exitosamente!", Toast.LENGTH_LONG);
                notification.show();
                finish();
            }else{
                Toast notification = Toast.makeText(this, "Ups, ocurrio un error, intente de nuevo!", Toast.LENGTH_LONG);
                notification.show();
            }

        }else{
            Toast notification = Toast.makeText(this, "Ups, las contraseñas no coinciden!", Toast.LENGTH_LONG);
            notification.show();
        }
    }

    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }


}
