package com.umg.maestria.umgnotas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Date;

import umgnotas.eis.dto.TbSede;
import umgnotas.service.TbSedeService;
import umgnotas.service.imp.TbSedeServiceImp;

public class Sede extends AppCompatActivity {

    private EditText sNom;
    private EditText sDir;
    private EditText sTel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sede);

        sNom = (EditText) findViewById(R.id.nomSede);
        sDir = (EditText) findViewById(R.id.dirSede);
        sTel = (EditText) findViewById(R.id.telSede);
    }

    public void guardarSede(View view){
        String nombreS = sNom.getText().toString();
        String direccionS = sDir.getText().toString();
        String telefonoS = sTel.getText().toString();

        //creo objeto sede e ingreso sus datos
        TbSede tbSede = new TbSede();
        tbSede.setSedCodigo(null);
        tbSede.setSedNombre(nombreS);
        tbSede.setSedDireccion(direccionS);
        tbSede.setSedTelefono(telefonoS);
        tbSede.setSedFechaApertura(new Date());
        tbSede.setSedUniCodigo(1);

        //llamo los servicios para guardar la sede
        TbSedeService tbSedeService = TbSedeServiceImp.getInstance();
        boolean guardado = tbSedeService.guardarSede(tbSede);
        if (guardado){
            finish();
            Intent ua = new Intent(this, UserAdmin.class);
            startActivity(ua);
            Toast notification = Toast.makeText(this, "Sede guardada exitosamente!", Toast.LENGTH_LONG);
            notification.show();
        }else{
            Toast notification = Toast.makeText(this, "Ups, ocurrio un error, intente de nuevo!", Toast.LENGTH_LONG);
            notification.show();
        }

    }



}
